/*
 * Project Afloat
 * Description: IOT network for sailboat
 * Author: Kenn Sebesta
 * Date: Jan 2020
 */

#include "Particle.h"

#include "Libraries/third_party/ArduinoJson-v6.13.0.h"  // https://arduinojson.org/

#include "Libraries/mesh_ack.h"

#include "gitcommit.h"

#if 1
// Configure debug output. Makes lots of console spew, but helps diagnose Mesh/Cloud connection failures
SerialDebugOutput debugOutput;
#endif


//! Local defines
#define SENSOR_NAME "bilge_pump"
#define DATA_FIELD_NAME "cycles"
#define DESCRIPTION "Monitors the bilge pump"

#define SAMPLE_INTERVAL "INT"
#define CONNECTION_INTERVAL 30
#define NUM_PENDING_ACKS 10

#define WATCHDOG_TIMEOUT_MS 60000

#define BATTERY_UPDATE_INTERVAL_S 60

#define LOW_BATTERY_VOLTAGE 3.4
#define CRITICAL_BATTERY_VOLTAGE 3.0
#define DEPLETED_BATTERY_VOLTAGE 2.8

#define ACK_TIMEOUT_S 5
#define MAX_NUMBER_OF_RETRIES 3

enum ACK_TUPLE_ENUM {
   TUPLE_ACKED,
   TUPLE_LAST_XMIT_TIME,
   TUPLE_NUMBER_OF_RETRIES,
   TUPLE_ACK_ID,
   TUPLE_PAYLOAD,
};

typedef std::tuple<time_t, uint32_t> payload_t;
typedef std::tuple<bool      /*TUPLE_ACKED*/,
                   time_t    /*TUPLE_LAST_XMIT_TIME*/,
                   uint32_t  /*TUPLE_NUMBER_OF_RETRIES*/,
                   uint8_t   /*TUPLE_ACK_ID*/,
                   payload_t /*TUPLE_PAYLOAD*/> ackTuple_t;

//! Local variables
static bool hasBilgePumpCycled = false;  // Indicates if the bilge pump has triggered
static bool mustSendProvisioning = true;
static ackTuple_t ackList[NUM_PENDING_ACKS];
static time_t cycleTime;
static time_t batteryUpdateInterval = BATTERY_UPDATE_INTERVAL_S;

MeshPacketWithAck sensorPayload(255);  // 255 is the max payload which can be sent with `Mesh.publish()`, C.f. https://docs.particle.io/reference/device-os/firmware/argon/#publish-

ApplicationWatchdog softwareWatchDog(WATCHDOG_TIMEOUT_MS, System.reset); // reset the system after 60 seconds if the application is unresponsive


//! Local functions

/**
 * @brief bilgePumpCycleInterrupt Bilge pump interrupt service routine
 */
void bilgePumpCycleInterrupt(){
   hasBilgePumpCycled = true;

   cycleTime = Time.now();
}


/**
 * @brief identityHandler Handles messages published to this node
 * @param event The published topic
 * @param jsonMsgPacket The published payload
 */
void identityHandler(const char *event, const char *jsonMsgPacket)
{
   Serial.printlnf("event=%s, payload=%s", event, jsonMsgPacket ? jsonMsgPacket : "NULL");

   // If the requester is asking for a provisioning statement...
   if (strcmp(jsonMsgPacket, "whoami") == 0) {
      // ... send one!
      mustSendProvisioning = true;
   } else if (strncmp(jsonMsgPacket, "ACK", 3) == 0) {
      int32_t ackedID = receiveAck(jsonMsgPacket[3]);

      for (int i=0; i<NUM_PENDING_ACKS; i++) {
         // If this ID is in the list, set it to false
         if (ackedID == std::get<TUPLE_ACK_ID>(ackList[i])) {
//            Serial.println(String(ackedID) + " removed from list at index " + String(i));
            std::get<TUPLE_ACKED>(ackList[i]) = true;
            break;
         }
      }
   }
}


/**
 * @brief setup Arduino setup routine. Runs once before launching into main().
 */
void setup()
{
   // Iterate over all ACK tuples and set the default ACKed state to `true`
   for (int i=0; i<NUM_PENDING_ACKS; i++) {
      std::get<TUPLE_ACKED>(ackList[i]) = true;
   }

   // Configure interrupts
   pinMode(D2, INPUT);
   attachInterrupt(D2, bilgePumpCycleInterrupt, RISING);

   pinMode(BTN, INPUT);
   attachInterrupt(BTN, bilgePumpCycleInterrupt, RISING);

   // Subscribe to all `whoami`
   Mesh.subscribe(SENSOR_NAME, identityHandler);
   Particle.publishVitals(60);  // Publish vitals every 60 seconds, indefinitely

   // Block until the time is valid
   while (Time.isValid() == false) {
      // Blocking wait. This isn't necessarily the smart thing to do, as the data still
      // has validity even if the time isn't set. However, it will take a fair amount of
      // care to ensure that the system handles an invalid time correctly, and so until
      // that is architected the safest thing to do is block.
   }
}

//#pragma GCC push_options
//#pragma GCC optimize ("O0")

/**
 * @brief publishProvisioningMessage Publishes the node's provisioning message
 */
void publishProvisioningMessage()
{
   // Create provisioning message
   const size_t capacity = JSON_ARRAY_SIZE(2) + JSON_OBJECT_SIZE(2) + JSON_OBJECT_SIZE(6);
   DynamicJsonDocument doc(capacity);

   doc["deviceName"] = SENSOR_NAME;
   doc["dataFieldName"] = DATA_FIELD_NAME;
   doc["description"] = DESCRIPTION;
   doc["sampleInterval"] = SAMPLE_INTERVAL;
   doc["connectionInterval"] = CONNECTION_INTERVAL;

   JsonObject firmware = doc.createNestedObject("firmware");
   firmware["date"] = __DATE__;  // `GIT_COMMIT_HASH` is auto-generated
   firmware["time"] = __TIME__;  // `GIT_COMMIT_HASH` is auto-generated
   firmware["branch"] = GIT_BRANCH_NAME;  // `GIT_BRANCH_NAME` is auto-generated
   firmware["hash"] = GIT_COMMIT_HASH;  // `GIT_COMMIT_HASH` is auto-generated

   // Convert document to string
   char output[255];  // 255 is the max payload which can be sent with `Mesh.publish()`, C.f. https://docs.particle.io/reference/device-os/firmware/argon/#publish-
   /* serializeMsgPack(doc, output); */
   serializeJson(doc, output);

   // Publish provisioning payload to cloud (not the mesh).
   bool isSuccessful = Particle.publish("sensor_provisioning", output, PRIVATE);

   // NOTE: this test is necessary because otherwise Particle.publish() is a non-blocking function, and the sleep might occur before the publish is
   // complete.
   if (!isSuccessful) {
      Serial.println("Unable to publish provisioning message to cloud");
      Serial.println(String("\t\t") + String(output));

   }
}


/**
 * @brief publishData Publish sensor data to the mesh
 * @param cycleTime The time at which the bilge pump ran
 * @param cycleCount The nth time the bilge pump has run
 * @param numberOfRetries The number of times the node has attempted to publish this tuple
 */
void publishData(time_t cycleTime, uint32_t cycleCount, uint32_t numberOfRetries = 0) {

   const size_t capacity = JSON_OBJECT_SIZE(2);
   DynamicJsonDocument sensorPacket(capacity);  // TODO: Change to static JSON document type
   sensorPacket["time"] = cycleTime;
   sensorPacket["cycleCount"] = cycleCount;

   Serial.print("\t\t");
   serializeJson(sensorPacket, Serial);
   Serial.println("");

   //Serialize the JSON and send it to the mesh/cloud
   /* serializeMsgPack(sensorPacket, (char *)outputAck.packet->data, bob.size); */
   serializeJson(sensorPacket, (char *)sensorPayload.packet->data, sensorPayload.size);

   uint8_t packetID;
   int32_t meshReturn = publishWithAck(SENSOR_NAME, sensorPayload, &packetID);

#if 1
   // =====================
   Particle.publish("bilge", (char *)sensorPayload.packet->data, PRIVATE);
   // =====================
#endif

   if (meshReturn != 0) {
      // get here if event publish did not work
      Serial.println("Mesh.publish() not successful? Returns error code: " + String(meshReturn));
   } else {
      // Iterate over all the elements in ackList
      for (int i=0; i<NUM_PENDING_ACKS; i++) {
         bool isACKed = std::get<TUPLE_ACKED>(ackList[i]);

         // If this element has been acked, then we can overwrite it
         if (isACKed == true) {
            // Get the current UTC time
            time_t tmpTime = Time.now();

            // Populate a fresh tuple
            ackList[i] = ackTuple_t(false, tmpTime, numberOfRetries, packetID, payload_t(cycleTime, cycleCount));

            break;
         }
      }
   }
}


/**
 * @brief printAckTuple Helper function to examine the array of ACK tuples
 * @param ackElement the ACK tuple to be printed.
 */
void printAckTuple(ackTuple_t ackElement)
{
   bool ack;
   time_t  lastXmitTime;
   uint32_t numberOfRetries;
   uint8_t  ackId;
   payload_t payload;

   // Do a left-hand assignment of the elements in the ACK tuple.
   std::tie(ack, lastXmitTime, numberOfRetries, ackId, payload) = ackElement;

   Serial.println("ACK: " + String(ack) + "   lastXmitTime: " + String(lastXmitTime) +
                  "   numberOfRetries: " + String(numberOfRetries) + "   ackId: " + String(ackId));
}


/**
 * @brief sleepUntil Sleep until a given UTC time, in [s]
 * @param untilUtc_s UTC time (in seconds) at which the processor should wake up.
 */
void sleepUntil(time_t untilUtc_s)
{
   // Get the current time
   time_t currentUtc_s = Time.now();

   // Ensure that the time is indeed in the future
   if (untilUtc_s > currentUtc_s) {
      // Compute the amount of time to sleep
      time_t sleepTime = untilUtc_s - currentUtc_s;

      System.sleep(D2, RISING, sleepTime);
   } else {
      // Do nothing and bail out.
      return;
   }
}


/**
 * @brief loop Inifinite loop. FreeRTOS equivalent to while(1){}.
 */
void loop()
{
   // Feed the watchdog
   softwareWatchDog.checkin();

   // Initialize defaults for sleep state output
   bool sleepTillInterrupted = true;
   bool sleepTillSyncTime = false;

   // Get the mesh up and running
   if (Mesh.ready() == false) {
      // Try to reconnect
      Mesh.connect();

      // Wait until mesh is ready
      const int32_t meshConnectionTimeout_ms = 2500;
      waitFor(Mesh.ready, meshConnectionTimeout_ms);

      // If the mesh is still not reconnected, go to sleep
      if (Mesh.ready() == false) {
         Serial.println("[MESH] Failed to connect, going to sleep");

         // Disconnect from the mesh
         Mesh.disconnect();
         // Wait an arbitrary 100ms as lead time for the Mesh commands to fully propagate
         delay(100);

         // Power cycle the mesh unit (in hopes it fixes things!)
         Mesh.off();
         // Wait an arbitrary 100ms as lead time for the Mesh commands to fully propagate
         delay(100);

         Mesh.on();
         // Wait an arbitrary 100ms as lead time for the Mesh commands to fully propagate
         delay(100);


         // Prep for going to sleep
         sleepTillSyncTime = true;
         goto sleep;
      }
   }


   //================================
   // HANDLE SENSOR EVENTS
   //================================

   // Check if the bilge pump has run.
   if (hasBilgePumpCycled == true) {
      static int cycleCount=0;

      // Publish data
      cycleCount++;
      publishData(cycleTime, cycleCount);

      // Turn off flag
      hasBilgePumpCycled = false;
   }

   // Periodically check battery.
   static time_t batteryUpdateTime_s = Time.now();
   if (batteryUpdateTime_s <= Time.now()) {
      float batteryVoltage = analogRead(BATT) * 0.0011224;
      bool publishRet = Particle.publish(String(SENSOR_NAME) + String("/battery"), String(batteryVoltage), PRIVATE);

      // NOTE: this test is necessary because otherwise Particle.publish() is a non-blocking function, and the sleep might occur before the publish is
      // complete.
      if (publishRet == true) {
         Serial.println("Successfully published battery report to cloud before sleep");
      }

      // If the battery is too low then be more aggressive about trying to sleep longer
      if (batteryVoltage < DEPLETED_BATTERY_VOLTAGE) {
         batteryUpdateInterval = BATTERY_UPDATE_INTERVAL_S * 8;
      } else if (batteryVoltage < CRITICAL_BATTERY_VOLTAGE) {
         batteryUpdateInterval = BATTERY_UPDATE_INTERVAL_S  * 4;
      } else if (batteryVoltage < LOW_BATTERY_VOLTAGE) {
         batteryUpdateInterval = BATTERY_UPDATE_INTERVAL_S * 2;
      } else {
         batteryUpdateInterval = BATTERY_UPDATE_INTERVAL_S;
      }

      // Advance next update time
      batteryUpdateTime_s += batteryUpdateInterval;
   }

   //================================
   // DO NETWORK HOUSEKEEPING
   //================================

   // Check if we should send provisioning
   if (mustSendProvisioning) {
      publishProvisioningMessage();
      mustSendProvisioning = false;
   }

   // Check if there are timed out packets which should be resent
   for (int i=0; i<NUM_PENDING_ACKS; i++) {
      bool isACKed = std::get<TUPLE_ACKED>(ackList[i]);
      if (isACKed == false) {
         time_t lastTransmitTime = std::get<TUPLE_LAST_XMIT_TIME>(ackList[i]);
         int32_t ellapsedTime = Time.now() - lastTransmitTime;

         // If the ACK has timed out
         if (ellapsedTime >= ACK_TIMEOUT_S) {
            // Abandon prior attempt
            haltAck(std::get<TUPLE_ACK_ID>(ackList[i]));

            std::get<TUPLE_ACKED>(ackList[i]) = true;

            uint32_t numberOfRetries = std::get<TUPLE_NUMBER_OF_RETRIES>(ackList[i]);

            // If we haven't tried too many times, then try again
            if (numberOfRetries <= MAX_NUMBER_OF_RETRIES) {
               // Extract data from tuple
               int cycleCount_old;
               time_t cycleTime_old;
               std::tie(cycleTime_old, cycleCount_old) = std::get<TUPLE_PAYLOAD>(ackList[i]);

               // Resend
               publishData(cycleTime_old, cycleCount_old, numberOfRetries);

               // Increment retry counter
               std::get<TUPLE_NUMBER_OF_RETRIES>(ackList[i]) += 1;

               Serial.println(String("[Mesh] Resending: ") + String(lastTransmitTime) + String("/") + String("(") + String(cycleTime_old) + String(",") +
                              String(cycleCount_old) + ") timed out after " + String(ellapsedTime) + " s, resending.");
            } else {
               Serial.println(String("[Mesh] Exceeded number of retries for ") + String(lastTransmitTime) + String(", ") + String(" timed out after ") + String(numberOfRetries) + " retries, abandoning.");
            }

         }
      }
   }


   //================================
   // ALL DONE, PREP FOR NEXT WAKE UP
   //================================

   // Loop over all members in the ACK list, in order to figure out if the node can go to sleep
   for (int i=0; i<NUM_PENDING_ACKS; i++) {
      /*printAckTuple(ackList[i]);*/

      bool isACKed = std::get<TUPLE_ACKED>(ackList[i]);

      // Check if any ACKs are pending
      if (isACKed == false) {
         sleepTillInterrupted = false;

         uint32_t numberOfRetries = std::get<TUPLE_NUMBER_OF_RETRIES>(ackList[i]);

         // Check if the number of retries is so high that we just need to go to sleep for a
         // while to try to resync with the mesh.
         if (numberOfRetries > MAX_NUMBER_OF_RETRIES) {
            sleepTillSyncTime = true;
            goto sleep;
         }
      }
   }

sleep:  // GOTO jump point
   // Check if the device should go to an unending sleep
   if (sleepTillInterrupted) {
      bool publishRet = Particle.publish(String(SENSOR_NAME) + String("/sleep"), String("Sleep until: INT (") + Time.timeStr() + String(")"), PRIVATE);

      // NOTE: this test is necessary because otherwise Particle.publish() is a non-blocking function, and the sleep might activate before the publish() is
      // complete.
      if (publishRet == true) {
         Serial.println("Successfully synced to the cloud before sleep");
      }

      System.sleep(D2, RISING);
   } else if (sleepTillSyncTime) {  // Check if we need to do a sync sleep, which allows the mesh node to resync with the nexus. This will only be needed if the mesh publish wasn't successful after multiple retries.
      static bool once = true;

      // This causes the sleepTillSync not to run on the first pass, which gives the
      // subsystem time to resolve any pending subscriptions. For instance, servicing a
      // full provisioning handshake takes some time after awaking from a sleep.
      if (once == true) {
         // 5 seconds is an arbitrary amount of time which seems long enough for any other
         // mesh actors to react and any Particle.publish() commands to fully propagate.
         delay(5000);
         return;
      } else {
         once = true;
      }

      /* Determine when to wake up. */
      static time_t wakeUpUTC_s = Time.now();  // Initialize wake up rhythm to current time

      // Make sure our wakeup time is in the future
      do {
         wakeUpUTC_s += CONNECTION_INTERVAL;
      } while(wakeUpUTC_s <= Time.now());

      bool publishRet = Particle.publish(String(SENSOR_NAME) + String("/sleep"), String("Sleep until: ") + Time.timeStr(wakeUpUTC_s), PRIVATE);

      // NOTE: this test is necessary because otherwise Particle.publish() is a non-blocking function, and the sleep might activate before the publish() is
      // complete.
      if (publishRet == true) {
         Serial.println("Successfully synced to the cloud before sleep");
      }

      sleepUntil(wakeUpUTC_s);
   } else {
      // 100ms delay to make sure the loop doesn't run too fast
      delay(100);
   }
}

//#pragma GCC pop_options
