/*
 * Project Afloat
 * Description: IOT network for sailboat
 * Author: Kenn Sebesta
 * Date: Jan 2020
 */

#include "Particle.h"

#include <vector>

#define ARDUINOJSON_ENABLE_ARDUINO_STRING 1  // https://arduinojson.org/v6/faq/how-to-use-the-string-class-on-particle/
#include "Libraries/third_party/ArduinoJson-v6.13.0.h"  // https://arduinojson.org/

#include "Libraries/mesh_ack.h"

#include "gitcommit.h"

#ifdef ELECTRON_TARGET
// Save the reset reason
// C.f.: https://docs.particle.io/reference/firmware/electron/#enablereset-
STARTUP(System.enableFeature(FEATURE_RESET_INFO));
STARTUP(System.enableFeature(FEATURE_RETAINED_MEMORY));

// Enable system threading
SYSTEM_THREAD(ENABLED);
SYSTEM_MODE(MANUAL);
#endif  // ELECTRON_TARGET

//! Local macros
#define MS_TO_BLE_INTERVALS(x)  (uint16_t)(x/0.625f + 0.5f)

//! Local defines
#define WATCHDOG_TIMEOUT_MS 6000

//! Local variables
static uint32_t loopCnt = 0;
DynamicJsonDocument docNodeList(4096);
String requestProvisioning;
std::vector<std::tuple<String, uint8_t>> ackList;

ApplicationWatchdog softwareWatchDog(WATCHDOG_TIMEOUT_MS, System.reset); // reset the system after 60 seconds if the application is unresponsive

//! Local functions

/**
 * @brief nodeDataHandler
 * @param event
 * @param jsonMsgPacket
 */
void nodeDataHandler(const char *event, const char *jsonMsgPacket)
{
   Serial.printlnf("event=%s, sensorData=%s", event, jsonMsgPacket ? jsonMsgPacket : "NULL");

   StaticJsonDocument<255> doc;
//      deserializeMsgPack(doc, jsonMsgPacket);
   deserializeJson(doc, jsonMsgPacket);

   // Is this a node which has already been seen?
   bool isNodeRegistered = docNodeList.containsKey(event);

   // If this is unknown, then request a provisioning packet
   if (isNodeRegistered == false) {
      // Set the string so that this can be handled outside the mesh handler
      requestProvisioning = String(event);

      // Print some nice console spew
      Serial.printlnf("deviceProvisioning=%s", event);

      Serial.print("\t\t");
      serializeJson(docNodeList, Serial);
      Serial.println();

      Serial.printlnf(String("`") + requestProvisioning + String("` unknown, requesting provisioning packet."));
   } else { // This node has been registered in the past, we can handle its data

      // ACK the message
      ackList.push_back(std::tuple<String, uint8_t>(String(event), jsonMsgPacket[0]));
   }
}


/**
 * @brief sensorProvisioningHandler Handles messages received on the provisioning topic
 * @param event Msg topic
 * @param jsonMsgPacket Msg payload. Should be in JSON format
 */
void sensorProvisioningHandler(const char *event, const char *jsonMsgPacket)
{
   static int i = 0;
   i++;
   Serial.print(i);
   Serial.print(", ");
   Serial.print(event);
   Serial.print(", payload: ");
   if (jsonMsgPacket) {
      StaticJsonDocument<255> doc;
//      deserializeMsgPack(doc, jsonMsgPacket);
      deserializeJson(doc, jsonMsgPacket);

      const char* deviceName = doc["deviceName"];
      const char* deviceDescription = doc["description"];

      Serial.println(jsonMsgPacket);

      if (strcmp(deviceName, "core") == 0) {
         Serial.println("This is the core");
         // Do nothing
         // .
         // .
         // .
      } else {
         Serial.println("This is a sensor node");
         Serial.println("\tname: `" + String(deviceName) + String("`"));
         Serial.println("\tdescription: " + String(deviceDescription));

         // Add sensor to known list
         docNodeList[std::string(deviceName)] = doc;
         Serial.print("\t");
         serializeJson(docNodeList, Serial);
         Serial.println("");

      }

   } else {
      Serial.println("NULL");
   }

   // Check if this is the core.
}


/**
 * @brief configureBLE Configures the BLE module
 */
void configureBLE()
{
   // Advertise every XXX milliseconds.
   BLE.setAdvertisingInterval(MS_TO_BLE_INTERVALS(500));

   BleAdvertisingData advData;

   // Name this core unit
   advData.appendLocalName("Nexus");

   // Write something useful in the advertising message
   const uint8_t customStringLength = 24;
   struct customData {
      uint16_t companyID;
      char     data[customStringLength + 1]; // Add an extra byte for the terminating NULL character
   } cd;

   cd.companyID = 0xBEEF;                            // undefined company ID for custom data

   // Write string to custom data field. This currently doesn't do anything useful, but leaves a framework
   // in place for a functioning BLE advertising feature.
   int retVal = snprintf(cd.data, customStringLength, "This is a nexus");

   // Check that the string isn't too large
   if (retVal < 0 || customStringLength < retVal) {
      strncpy(cd.data, "Custom data too long!", customStringLength);
   }

   // Append the string to the advertising struct
   advData.appendCustomData((uint8_t*)&cd, 2 + strlen(cd.data));

   // Start continuously advertising (when not connected)
   BLE.advertise(&advData);
}


/**
 * @brief setup Arduino setup routine. Runs once before launching into main().
 */
void setup()
{
   Serial.println("Booting Afloat Nexus...");

   // Pre-allocate space for 100 ACK messages
   ackList.reserve(100);

   // only events from my devices
   Particle.subscribe("sensor_provisioning", sensorProvisioningHandler, MY_DEVICES);

   bool isSuccessful;
   isSuccessful = Particle.publish("nexus_provisioning", "core", PRIVATE);
   if (!isSuccessful) {
      // get here if event publish did not work
   }

   // Subscribe to all mesh messsages
   Mesh.subscribe("", nodeDataHandler);

   // Enable BLE
   configureBLE();

   // Block until the time is valid
   while (Time.isValid() == false) {
      // Blocking wait. This isn't necessarily the smart thing to do, as the data still
      // has validity even if the time isn't set. However, it will take a fair amount of
      // care to ensure that the system handles an invalid time correctly, and so until
      // that is architected the safest thing to do is block.
   }

   /* DEBUG output to indicate setup completed */
   Serial.println("Afloat Nexus booted. Entering into main loop...");
}


/**
 * @brief loop Inifinite loop. FreeRTOS equivalent to while(1){}.
 */
void loop()
{
   // Feed the watchdog
   softwareWatchDog.checkin();

   // Increment loop counter
   loopCnt++;

   // Check if there are any pending provisioning requests
   if (requestProvisioning.length() > 0) {
      Serial.printlnf("request provisioning from " + requestProvisioning);
      Mesh.publish(requestProvisioning, "whoami");
      requestProvisioning = "";
   }

   // Loop over the ACK list and publish all ACKs (to the Mesh)
   if (ackList.size() > 0) {
      for (const auto &i : ackList) {
         uint8_t id[5] = {'A', 'C', 'K', std::get<1>(i), 0};
         String nameStr =  std::get<0>(i);

         char name[64];
         nameStr.toCharArray(name, 63);
         Mesh.publish((const char *)name, (const char *)&id);

         Serial.print("ACK'ed name: " + String(name));
         Serial.println(", id: " + String(id[3]));
      }

      ackList.clear();
   }

   // Try to communicate with the cloud
   bool isCloudPublishSuccessful = Particle.publish("I'm not dead yet!", PRIVATE);

   // Check if we successfully published to the cloud
#define NUM_CLOUD_RETRIES
#define TIME_BEFORE_CLOUD_RETRY
   if (isCloudPublishSuccessful == false) {
      // Test if we have a cloud connection
      // Enqueue this message for later transmission
   }

   // Sleep
#if 1
   delay(1*1000);
#else
   // This can't be implemented until there is an RTC or some other architecture
   // for an input signal on the board.
   System.sleep(D1, RISING, 3600);
#endif
}
