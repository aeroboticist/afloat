/*
 * Project Afloat
 * Description: IOT network for sailboat
 * Author: Kenn Sebesta
 * Date: Jan 2020
 */

#include "Particle.h"

#include "Libraries/mesh_ack.h"

#include "gitcommit.h"

#include "BilgeSensor.h"

#if 0
// Configure debug output. Makes lots of console spew, but helps diagnose Mesh/Cloud connection failures
SerialDebugOutput debugOutput;
#endif


//! Local defines
#define SENSOR_NAME "bilge_sensor"
#define DATA_FIELD_NAME "cycles"
#define DESCRIPTION "Monitors the bilge pump"

#define BILGE_SENSOR_PIN D2
#define BILGE_SENSOR_INTERRUPT_MODE RISING

#define WAKE_PINS {BILGE_SENSOR_PIN}
#define WAKE_INTERRUPT_MODES {BILGE_SENSOR_INTERRUPT_MODE}
#define CONNECTION_INTERVAL_S 30
#define MAX_NUM_PENDING_ACKS 10

#define WATCHDOG_TIMEOUT_MS (60*1000)


//! Local variables
ApplicationWatchdog softwareWatchDog(WATCHDOG_TIMEOUT_MS, System.reset); // reset the system after 60 seconds if the application is unresponsive

BilgeSensor bilgeSensor(String(SENSOR_NAME),
                        String(DATA_FIELD_NAME),
                        String(DESCRIPTION),
                        WAKE_PINS,
                        WAKE_INTERRUPT_MODES,
                        CONNECTION_INTERVAL_S,
                        MAX_NUM_PENDING_ACKS);

static bool hasBilgePumpCycled = false;  // Indicates if the bilge pump has triggered
static time_t measurementTime;

//! Local functions

/**
 * @brief bilgePumpCycleInterrupt Bilge pump interrupt service routine
 */
void bilgePumpCycleInterrupt(){
   hasBilgePumpCycled = true;

   measurementTime = Time.now();
}

/**
 * @brief identityHandler Handles messages published to this node
 * @param event The published topic
 * @param byteArrayPacket The published payload
 */
void identityHandler(const char *event, const char *byteArrayPacket)
{
   // DO NOTHING ELSE BUT THE BELOW CALL TO THE SUBCLASSED IDENTITYHANDLER.
   // TODO: FIGURE OUT HOW TO PASS IDENTITYHANDLER AS A FUNCTION POINTER. IT'S MORE OBSCURE THAN YOU THINK.
   bilgeSensor.identityHandler(event, byteArrayPacket);
}


/**
 * @brief setup Arduino setup routine. Runs once before launching into main().
 */
void setup()
{
   Serial.println("Booting node...");

   // Subscribe to all `whoami`
   Mesh.subscribe(SENSOR_NAME, identityHandler);
   Particle.publishVitals(60);  // Publish vitals every 60 seconds, indefinitely

   // Block until the time is valid
   while (Time.isValid() == false) {
      // Blocking wait. This isn't necessarily the smart thing to do, as the data still
      // has validity even if the time isn't set. However, it will take a fair amount of
      // care to ensure that the system handles an invalid time correctly, and so until
      // that is architected the safest thing to do is block.
   }

   /*
    * Perform any extra startup routines here.
    * .
    * .
    * .
    */

   // Configure interrupts
   pinMode(BILGE_SENSOR_PIN, INPUT);
   attachInterrupt(BILGE_SENSOR_PIN, bilgePumpCycleInterrupt, BILGE_SENSOR_INTERRUPT_MODE);


   /* DEBUG output to indicate setup completed */
   Serial.println("Node booted. Entering into main loop...");
}



/**
 * @brief loop Inifinite loop. FreeRTOS equivalent to while(1){}.
 */
void loop()
{
   bool shouldDelaySleep = false;
   bool shouldSleepImmediately = false;

   // Initialize defaults for sleep state output
   static enum SLEEP_STATE sleepState = SLEEP_DONT_SLEEP;

   // Feed the watchdog
   softwareWatchDog.checkin();

   // Run preliminary loop code
   bilgeSensor.beginLoop(&sleepState, &shouldSleepImmediately);

   // Check if we should jump immediately to sleep
   if (shouldSleepImmediately == true) {
      Serial.println("Going to sleep immediately...");

      goto sleep;
   }


   //================================
   // HANDLE SENSOR EVENTS
   //================================

   // Check if the bilge pump has run.
   if (hasBilgePumpCycled == true) {
      static int cycleCount=0;

      // Publish data
      cycleCount++;
      bilgeSensor.publishData(measurementTime, cycleCount);

      // Turn off flag
      hasBilgePumpCycled = false;

      // Set the sleep state
      sleepState = SLEEP_TILL_INTERRUPTED;
   }

   //================================
   // DO GENERIC HOUSEKEEPING
   //================================

   // Check the battery
   bilgeSensor.updateBatteryState();

   // Do network housekeeping
   bilgeSensor.performNetworkHousekeeping(&sleepState, &shouldDelaySleep);

   // Check if we should delay sleeping
   if (shouldDelaySleep == true) {
      // 1000ms delay to make sure the loop doesn't run too fast. Too many publishes per second overwhelms things
      Serial.println("[MESH] Delaying sleep...");
      delay(1000);
   } else {
sleep:  // GOTO jump point
      switch  (sleepState) {
      case SLEEP_TILL_NEXT_SAMPLE:
      case SLEEP_TILL_INTERRUPTED:
      case SLEEP_TILL_SYNC_TIME:
         bilgeSensor.handleSleep(sleepState);
         break;
      case SLEEP_UNDETERMINED:
      case SLEEP_DONT_SLEEP:
         // 1000ms delay to make sure the loop doesn't run too fast. Too many publishes per second overwhelms things
         delay(1000);
         break;
      }
   }
}
